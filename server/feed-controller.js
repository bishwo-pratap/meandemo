var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');
var mkpath = require('mkpath');
var Feed = require('../models/feeds');
var Like = require('../models/likes');


module.exports = {
	//API method to upload image
	postFeedImage: function(req, res) {
		console.log(req.files);
		var file 	 	= req.files.file,
			id 		 	= req.body.userId,
			tempPath 	= file.path,
			uploadDate  = new Date().getTime(),
			dir 	    = path.join(__dirname + "/../uploads/feeds/"+id+"/"+uploadDate+"/");

		// create directory if not exist
		mkpath(	dir, function (err) {
		    if (err) throw err;
		    console.log('Directory structure '+ dir +' created');
		    fs.rename(tempPath, dir+file.name, function(err){
				if(err) {
					res.json({
						msg    : "Error Uploading Image",
						status : 500
					});
				}

				console.log("Image Upload Successful");
				res.send({
					uploadDate : uploadDate,
					status	   : 200
				});
			});
		});
	},

	insertFeed: function(req,res) {
		var feed = new Feed(req.body);

		if(feed.save()){
			res.json({
				data   : req.body,
				msg    : 'Feed Inserted Successfully',
				status : 200
			})
		} else {
			res.json({
				msg    : 'Error Inserting Feed',
				status : 500
			});
		}
	},

	getFeeds: function(req, res) {
		if(req.params.id)
			var id = req.params.id;
		var matchCondition = (typeof(id) != "undefined") ? { _id : mongoose.Types.ObjectId(id) } : { };

		Feed.aggregate([
			{
				$lookup: {
					from		 : 'Likes',
					localField   :"_id",
					foreignField : "pid",
					as			 : "likes"
				}
			},
			{
				$lookup: {
					from		 : 'Comments',
					localField   : '_id',
					foreignField : 'pid',
					as  		 : "comments"
				}
			},
			{ $sort: {created_at:-1} },
			{ $match : matchCondition }
		], function(err, result){
			if(err && result.length === 0){
				console.log("No records Found:"+err);
				res.json({
					status : 500,
					msg    : 'No Records'
				});
			} else {
				console.log("Records Found:::"+result);
				res.json({
					status : 200, 
					msg	   : 'Records Found',
					data   : result
				});
			}
		});
	},

	like: function(req, res){
		var like = new Like(req.body);
		if(like.save()){
			res.json({status: 200});
		} else {
			res.json({status: 500});
		}
	},
	
	unlike: function(req, res){
		Like.remove(req.body, function(err, result){
			if(err)
				res.send({
					status:500, 
					message: "Failed deleting document!"
				});
			res.send({
				status:200,
				message: "Successfully unliked"
			})
		});
	},

	delete : function(req, res){
		Feed.remove({ _id : mongoose.Types.ObjectId(req.body.id) }, function(err, result){
			if(err)
				res.send({
					status:500, 
					message: "Failed deleting feed!"
				});
			res.send({
				status:200,
				message: "Successfully deleted feed!"
			})
		});
	},

	updateFeed: function(req, res){
		var condition = {_id : mongoose.Types.ObjectId(req.body.id)};

		delete req.body.id

		Feed.update(condition, req.body, function(err, result){
			if(err)
				res.send({
					status:500, 
					message: "Failed updating feed!"
				});
			res.send({
				status:200,
				message: "Successfully updated feed!"
			})
		});

	}

};