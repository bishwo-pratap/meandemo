var mongoose = require('mongoose');

var Comment  = require('../models/comments');

module.exports = {
	comment : function(req, res){
		var comment = new Comment(req.body);
		if(comment.save()){
			res.json({
				status:200
			})
		} else {
			res.json({
				status: 500
			});
		}
	},

	getComment(req, res){
		Comment.find({_id: mongoose.Types.ObjectId(req.params.id)}, function(err, result){
			if(err)
				res.send({
					status:500, 
					message: "Failed retriving comment!"
				});
			res.send({
				status:200,
				data: result,
				message: "Successfully retrieved comment!"
			})
		});
	},

	update: function(req, res){
		var condition = {_id : mongoose.Types.ObjectId(req.body.id)};

		delete req.body.id

		Comment.update(condition, req.body, function(err, result){
			if(err)
				res.send({
					status:500, 
					message: "Failed updating comment!"
				});
			res.send({
				status:200,
				message: "Successfully updated comment!"
			})
		});

	},

	delete : function(req, res){
		Comment.remove({ _id : mongoose.Types.ObjectId(req.body.id) }, function(err, result){
			if(err)
				res.send({
					status:500, 
					message: "Failed deleting comment!"
				});
			res.send({
				status:200,
				message: "Successfully deleted comment!"
			})
		});
	}
};