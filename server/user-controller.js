var mongoose = require('mongoose');

var User 	 = require('../models/users');

module.exports = {
	login: function(req, res){
		console.log('Inside user controller');

		User.find(req.body, function(err, result){
			if(err){
				res.json({status : 500, msg : err});
			}
			else if(result && result.length === 1){
				var userData = result[0],
					resp     = {
						id   : userData._id,
						email: userData.email
					};

					if(userData.username)
						resp.username = userData.username;

					res.json({status : 200, data : resp});
			}
		})

	},

	signUp: function(req, res){
		User.find({email: req.body.email}, function(err, result){
			if(!err && result.length === 0){
				var user = new User(req.body);
				user.save();
				var userData = {
					email: req.body.email,
					password: req.body.password,
					// _id  : user._id
				};
				res.json({status : 200, error: false,  data : userData});
			} else {
				res.json({status : 500, error: true, msg  : 'User Already Exists'});
			}
		});
	}
};