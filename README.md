# README #

### What is this repository for? ###

* This repository is made for the fulfilment of guideline requirements provided for technical interview.

### How do I get set up? ###

* Clone the project in desired folder
* Open terminal and run: nodemon index.js
* visit localhost:3000 on your browser

### Functionalities ###

* Restful APIs
* MEAN(nodejs as backend, angular as frontend, mongo as db)
* Simple application with post and comments
* Add post, edit post, view all post and view a single post.
* Add comments to post, edit comments to post, delete comments to post, view comments regarding the post.
* User authentication

## Postman API Tests ##

### Users ###

#### POST ####

#### Register ####

#### URL 	     :
localhost:3000/api/user/signup

####Body (raw, JSON(application/json))####
{
      "email": "admin",
      "username": "admin",
}

### Feeds ###

#### POST ####

#### URL 	     :
localhost:3000/api/feeds/insertFeed

#### Body (raw, JSON(application/json)) ####
{
      "created_by": "< user._id >",
      "feed": "post API test",
      "created_at": "1490174426251",
      "updated_at": "1490174426251",
      "created_by_name": "admin",
}

#### GET ####
#### URL 	     :
localhost:3000/api/feeds/getFeeds
#### URL 	     :
localhost:3000/api/feeds/getFeed/< feeds._id >

#### PUT ####
#### URL 	     :
localhost:3000/api/feeds/updateFeed

#### Body (raw, JSON(application/json)) ####
{
	"feed" : "Testing PUT",
	"id" : "< feeds._id >"	
}

#### DELETE ####
#### URL 	     :
localhost:3000/api/feeds/delete

#### Body (raw, JSON(application/json)) ####
{
	"id" : "< feeds._id >"	
}

### Likes ###

#### POST ####

#### URL 	     :
localhost:3000/api/feeds/like

#### Body (raw, JSON(application/json)) ####
{
      "uid": "< user._id >",
      "pid": "< post._id >",
      "like": true,
      "time": "1490174426251"
}


#### DELETE ####
#### URL 	     :
localhost:3000/api/feeds/unlike

#### Body (raw, JSON(application/json)) ####
{
	"uid": "< user._id >",
	"pid": "< post._id >",
}


### Comments ###

#### POST ####

#### URL 	     :
localhost:3000/api/comment

#### Body (raw, JSON(application/json)) ####
{
	"uid": "< user._id >",
	"pid": "< feed._id >",
	"comment": "API comment test",
	"created_at" : "1490869746544",
	"username" : "user_name"
}

#### GET ####
#### URL 	     :
localhost:3000/api/comment/getComment/< user._id >

#### PUT ####
#### URL 	     :
localhost:3000/api/comment/update

#### Body (raw, JSON(application/json)) ####
{
	"comment" : "Testing PUT comment",
	"id" : "< comment._id >"	
}

#### DELETE ####
#### URL 	     :
localhost:3000/api/comment/delete

#### Body (raw, JSON(application/json)) ####
{
	"id" : "< comment._id >"	
}