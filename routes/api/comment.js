var  express = require('express');
var router   = express.Router();

/*model*/
var Comments = require('../../models/comments');

/*controller*/
var commentController = require('../../server/comment-controller.js');

/*API routes*/
router.post('/', commentController.comment)
router.delete('/delete', commentController.delete)
router.get('/getComment/:id',commentController.getComment);
router.put('/update', commentController.update)



/*exporting router*/
module.exports = router;