var  express = require('express');
var router   = express.Router();

/*model*/
var Feeds = require('../../models/feeds');

/*controller*/
var feedController = require('../../server/feed-controller.js');

// Routes
router.get('/getFeeds',feedController.getFeeds);
router.get('/getFeed/:id',feedController.getFeeds);

router.post('/postFeedImage', feedController.postFeedImage);
router.post('/insertFeed', feedController.insertFeed);
router.post('/like', feedController.like);

router.put('/updateFeed',feedController.updateFeed);

router.delete('/unlike', feedController.unlike);
router.delete('/delete', feedController.delete);

/*exporting the api methods*/
module.exports = router;