var express = require('express');
var router  = express.Router();

//model
var User = require('../../models/users'); 

//controller
var userController = require('../../server/user-controller.js')

//API methods
router.post('/login', userController.login);
router.post('/signup', userController.signUp);

//exporting router methods
module.exports = router;