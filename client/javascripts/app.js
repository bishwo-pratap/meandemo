var app = angular.module('MEANSocial',['ui.router','ngFileUpload']);

app.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('home');

	$stateProvider
	.state('signup',{
		url:'/signup',
		controller:'SignupController',
		templateUrl: 'views/app/signup.html'
	})
	.state('home',{
		url:'/home',
		controller:'FeedsController',
		templateUrl: 'views/partial-home.html'
	})
	.state('feed',{
		url: '/feed/{feedId}',
		controller:'FeedsController',
		templateUrl: 'views/feed/feed.html'
	})
	.state('editFeed',{
		url: '/editFeed/{updateId}',
		controller:'FeedsController',
		templateUrl: 'views/feed/update.html'
	})
	.state('editComment',{
		url: '/comment/{commentId}',
		controller:'CommentController',
		templateUrl: 'views/comment/update.html'
	})
	
});

