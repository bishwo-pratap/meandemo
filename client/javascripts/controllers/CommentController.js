app.controller('CommentController',['$scope', '$http', '$location', 'CommentService', '$stateParams', function($scope, $http, $location, CommentService, $stateParams){
	$scope.user  	  = {};
	$scope.comments   = {};
	$scope.commentId  = $stateParams.commentId;

	if(localStorage['User-Data']){
		$scope.user  = JSON.parse(localStorage['User-Data']).data.data || undefined;
	}

	/*function to load feeds*/
	var loadComment = function() {
		if($scope.commentId){
			CommentService.loadComment($scope.commentId , function(data){
				$scope.comments = data.data.data[0];
			});
		}
	}
	if($scope.commentId){
		loadComment();
	}

	$scope.comment = function(){
		if(($scope.user.id) && $scope.comments){
			var user = $scope.user;
				reqParams = {
					uid 	   : user.id,
					pid 	   : $scope.comments.pid,
					comment    : $scope.comments.comment,
					created_at : new Date().getTime()
				};
				if(user.username)
					reqParams.uname = user.username;
				else if(user.email)
					reqParams.uname = user.email;

			CommentService.comment(reqParams, function(data){
				if(data.success){
					location.reload();
				}
			});
		}
	}

	$scope.update = function(){
		if(($scope.user.id) && $scope.comments){
			var user = $scope.user;
				reqParams = {
					id 			: $scope.comments._id,
					comment 	: $scope.comments.comment,
					pid 		: $scope.comments.pid,
					updated_at  : new Date().getTime()
				};
			CommentService.update(reqParams, function(data){
				$location.path('feed/'+reqParams.pid);
			});
		}
	}

}]);