app.controller('SignupController',['$scope', '$location', '$rootScope', '$state', '$http', 'UserService', function($scope, $location, $rootScope, $state, $http, UserService){
		$scope.newUser = {};
	var login = function(success){
		$http.post('/api/user/login', success.data).then(
			function(success){

				$rootScope.uname = success.data.data.email.charAt(0).toUpperCase() + success.data.data.email.slice(1);;

				localStorage.setItem('User-Data', JSON.stringify(success));
				$rootScope.isLoggedIn = true;
				$location.path('/home');
			},
			function(faliure){
				console.log('Faliure: '+faliure[0]);
			}
		);
	}

	$scope.createUser = function(){

		UserService.signup($scope.newUser, function(data){
			if(data.success){
					login(data);
			}
			console.log(data.msg+" : "+data.error);
		});
	}


}]);

