app.controller('FeedsController',['Upload', '$scope','$http', 'FeedService', '$rootScope', '$stateParams', '$location', function(Upload, $scope, $http, FeedService, $rootScope, $stateParams, $location){
	/*variales*/
	$scope.feeds      = [];
	$scope.feed       = {};
	$scope.user  	  = {};
	$scope.single 	  = false;
	$scope.singleFeed = $stateParams.feedId;
	$scope.feedId     = $stateParams.updateId;

	if(localStorage['User-Data']){
		$scope.user  = JSON.parse(localStorage['User-Data']).data.data || undefined;
	}
	/*function to load feeds*/
	var loadFeeds = function() {
		var id = (typeof($scope.singleFeed) != 'undefined') ? $scope.singleFeed : (typeof($scope.feedId) != 'undefined') ? $scope.feedId : null;

		if($scope.singleFeed || $scope.feedId){
			FeedService.loadFeeds(id , function(data){
				if (typeof($scope.feedId) != 'undefined') {
					$scope.feed 	  = data.data.data[0];
					$scope.feed.tweet = data.data.data[0].feed;
				} else{ 
					$scope.single 	  = true;
					$scope.feeds = data.data.data;
				}
			});
		} else {
			FeedService.loadFeeds(id ,function(data){
				$scope.feeds = data.data.data;
			});
		}
	}

	loadFeeds();

	/*function to post feeds*/
	$scope.postFeed = function() {
		if(($scope.user) && ($scope.feed.image || $scope.feed.tweet)){
			$scope.feed.image = $scope.feed.image ? $scope.feed.image : null;
			$scope.feed.tweet = $scope.feed.tweet ? $scope.feed.tweet : null; 

			if($scope.feed.image){

				FeedService.upload($scope.user.id, $scope.feed, function(data){
					if(data.success){
						insertFeedDetails(data.date);
						$scope.feed.image = null;
						$scope.feed.tweet = null;	
					}
				});
			} else {
				console.log('No Image Being Uploaded');
				insertFeedDetails();
				$scope.feed.image = null;
				$scope.feed.tweet = null;
			}
		}
	}

	/*function to update feeds*/
	$scope.updateFeed = function() {
		if($scope.user && $scope.feed.tweet){
			
			$scope.feed.tweet = $scope.feed.tweet ? $scope.feed.tweet : null; 
			FeedService.updateFeed($scope.feed, function(data){
				if(data.success){
					var id = data.id;
					$location.path('feed/'+id);			
				}
			});
		}
	}

	/*function to insert feeds*/
	var insertFeedDetails = function(created_at) {
		
		var date = (created_at === undefined) ? new Date().getTime() : created_at,
			userData   = $scope.user;
		var request = {
				created_by : userData.id,
				feed 	   : $scope.feed.tweet,
				created_at : date, 
				updated_at : date 
		};

		if($scope.feed.image)
			request.image = $scope.feed.image.name;

		if(userData.username)
			request.created_by_name = userData.username;
		else
			request.created_by_name = userData.email;

		FeedService.insertFeed(request, function(data){
			if(data.success){
				console.log("Reloading Feeds");
				loadFeeds();
			}
		});
	}

	/*delete feed*/
	$scope.deleteFeed =function(feed_id){
		if($scope.user){
			var request = { 
				id  : feed_id,
			};
			
			FeedService.deleteFeed(request, function(data){
				if(data.success){
					console.log("Reloading Feeds");
					loadFeeds();
				}
			});
		}
	}

	/*like*/
	$scope.like = function(post_id){
		if($scope.user.id){
			var request = { 
				time : new Date().getTime(),
				uid  : $scope.user.id,
				pid  : post_id,
				like : '1'
			};

			FeedService.like(request, function(data){
				if(data.success){
					console.log("Reloading Feeds");
					loadFeeds();
				}
			});
		}
	}

	/*unlike*/
	$scope.unlike =function(post_id){
		if($scope.user.id){
			var request = { 
				uid  : $scope.user.id,
				pid  : post_id,
			};
			FeedService.unlike(request, function(data){
				if(data.success){
					console.log("Reloading Feeds");
					loadFeeds();
				}
			});
		}
	}

	/*delete comment*/
	$scope.deleteComment =function(comment_id){
		if($scope.user.id){
			var request = { 
				id  : comment_id,
			};
			
			FeedService.deleteComment(request, function(data){
				if(data.success){
					console.log("Reloading Feeds");
					loadFeeds();
				}
			});
		}
	}

	
}]);