app.controller('LoginController',['$scope', '$http', '$location', '$rootScope', function($scope, $http, $location, $rootScope){

	if(localStorage['User-Data']){
		var userData 		  = JSON.parse(localStorage['User-Data']).data.data;

		$rootScope.isLoggedIn = true;
		$rootScope.uname 		  = getUsername(userData);
	} else {
		$rootScope.isLoggedIn = false;
		$rootScope.uname 		  = null;
	}

	function getUsername(data){
		if(data.username)
			return data.username.charAt(0).toUpperCase() + data.username.slice(1);
		else if (data.email)
			return data.email.charAt(0).toUpperCase() + data.email.slice(1);
	}

	$scope.login = function(){
		$http.post('/api/user/login',$scope.user).then(
			function(response){
				$rootScope.uname = getUsername(response.data.data);
				localStorage.setItem('User-Data',JSON.stringify(response));
				location.reload();
			},
			function(faliure){
				$rootScope.isLoggedIn= false;
				console.log('Error:'+faliure);
			}
		);
	}

	$scope.logout = function() {
		localStorage.clear();
		if(typeof($scope.user) != "undefined"){
			$scope.user.email     = null;
			$scope.user.password  = null;
		}
		location.reload();
	}

}]);