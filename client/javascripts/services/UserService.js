app.service('UserService',['$http', function($http){
	this.signup = signup;

	function signup(request, callback){
		$http.post('/api/user/signup', request)
		.then(function(data){
			var response = data.data;
			if(response.error){
				var resp = { success : false, msg : " Registration failed", error : response.msg };
				callback(resp);
			} else {
				
				var resp = { success : true, data: response.data , msg : "Registered successfully" };
				callback(resp);
			}
		}, function(err){
			console.log(err);
		});	
	}

}]);