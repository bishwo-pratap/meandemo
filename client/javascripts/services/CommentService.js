app.service('CommentService',['$http', function($http){
	this.loadComment = loadComment;
	this.comment 	 = comment;
	this.update 	 = update;

	function loadComment(id, callback){
		$http.get('/api/comment/getComment/'+id)
		.then(function(result){
			callback(result);
		},
		function(err){
			console.log(err);
		});
	}

	function comment(request, callback){
		$http.post('/api/comment',request)
		.then(function(){
			var resp = { success : true, msg : " Commented successfully"};
			callback(resp);
		}, function(err){
			console.log(err);
			var resp = { success : false, msg : " Commenting failed", error : err};
			callback(resp);
		});
	}

	function update(request, callback){
		$http.put('/api/comment/update',request)
		.then(function(){
			var resp = { success : true, msg : " Commented successfully"};
			callback(resp);
		}, function(err){
			console.log(err);
			var resp = { success : false, msg : " Commenting failed", error : err};
			callback(resp);
		});	
	}

}]);