var app = angular.module('MEANSocial');

app.service('FeedService',['$http', 'Upload', function($http, Upload){
	this.loadFeeds  	= loadFeeds;
	this.upload 		= upload;
	this.updateFeed 	= updateFeed;
	this.insertFeed 	= insertFeed;
	this.deleteFeed 	= deleteFeed;
	this.like 			= like;
	this.unlike 		= unlike;
	this.deleteComment 	= deleteComment;

	/*Feeds*/
	function loadFeeds(id,callback){
		var url = (id !== null) ? '/api/feeds/getFeed/'+id : '/api/feeds/getFeeds';

		$http.get(url)
		.then(function(result){
			angular.forEach(result.data.data, function(value, key){
				var liked = false;
				angular.forEach(value, function(val, key){
					if(key=="likes"){
						if(typeof(val) != 'undefined' && typeof(localStorage['User-Data']) != 'undefined'){
							var userData = JSON.parse(localStorage['User-Data']);
							angular.forEach(val, function(likesVal, likesKey){
								if(likesVal.uid == userData.data.data.id){
									value.liked = true;
								} else {
									value.liked = false;
								}
							});
						}
					}
				});
			});
			callback(result);
		},
		function(err){
			console.log(err);
		});
	}

	function upload(id, feed, callback){
		Upload.upload({
			url	   : 'api/feeds/postFeedImage',
			method : 'POST',
			data   : {
				userId : id
			},
			file   : feed.image
		})
		.progress(function(evt){
			console.log('firing postFeed() function');
		})
		.success(function(data){
			console.log('Image Being Uploaded');
			var resp = { success : true, date : data.uploadDate , msg : "Image inserted successfully"};
			callback(resp);
		})
		.error(function(err){
			console.log(err);
			var resp = { success : false, msg : "Image not inserted"};
			callback(resp);
		});
	}

	function updateFeed(feed, callback){
		var request = {
				id 		   : feed._id,
				feed 	   : feed.tweet,
				updated_at : new Date().getTime() 
		};
		$http.put('api/feeds/updateFeed', request)
		.then(function(){
			var resp = { success : true, id : feed._id, msg : "Feed updated successfully"};
			callback(resp);
		}, function(err){
			console.log(err);
			var resp = { success : false, msg : "Feed not updated", error : err};
			callback(resp);
		});
	}

	function insertFeed(feed, callback){
		$http.post('api/feeds/insertFeed', feed)
		.then(function(){
			var resp = { success : true, msg : "Feed inserted successfully"};
			callback(resp);
		}, function(err){
			var resp = { success : false, msg : "Feed not inserted", error : err};
			callback(resp);
		});
	}

	function deleteFeed(feed, callback){
		$http({
		    method: 'DELETE',
		    url: 'api/feeds/delete',
		    data: feed,
		    headers: {'Content-Type': 'application/json;charset=utf-8'}
		})
		.then(function(){
			var resp = { success : true, msg : "Feed deleted successfully"};
			callback(resp);
		}, function(err){
			var resp = { success : false, msg : "Feed not deleted", error : err};
			callback(resp);
		});
	}

	/*Likes*/
	function like(request, callback){
		$http.post('api/feeds/like', request)
		.then(function(){
			var resp = { success : true, msg : "Feed liked successfully"};
			callback(resp);
		}, function(err){
			var resp = { success : false, msg : "Feed not liked", error : err};
			callback(resp);
		});
	}

	function unlike(request, callback){
		$http({
		    method: 'DELETE',
		    url: 'api/feeds/unlike',
		    data: request,
		    headers: {'Content-Type': 'application/json;charset=utf-8'}
		})
		.then(function(){
			var resp = { success : true, msg : "Feed unliked successfully"};
			callback(resp);
		}, function(err){
			var resp = { success : false, msg : "Feed not unliked", error : err};
			callback(resp);
		});
	}

	/*Comment*/
	function deleteComment(request, callback){
		$http({
		    method: 'DELETE',
		    url: 'api/comment/delete',
		    data: request,
		    headers: {'Content-Type': 'application/json;charset=utf-8'}
		})
		.then(function(){
			var resp = { success : true, msg : "Comment deleted successfully"};
			callback(resp);
		}, function(err){
			var resp = { success : false, msg : "Comment not deleted", error : err};
			callback(resp);
		});
	}

}]);