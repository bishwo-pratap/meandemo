/*Importing necessary dependencies*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

// api
var feedsApi = require('./routes/api/feeds');
var userApi = require('./routes/api/user');
var commentApi = require('./routes/api/comment');


//initializing app
var app = express();

// using connect-multiparty for file upload
app.use(multipartMiddleware);

// setting static variables to be accessed from the html
app.use('/views', express.static(__dirname +"/views"));
app.use('/client', express.static(__dirname +"/client"));
app.use('/node_modules', express.static(__dirname +"/node_modules"));
app.use('/uploads', express.static(__dirname +"/uploads"));

//connecting to mongoose
mongoose.connect('mongodb://localhost:27017/MEANSocial');


//using parsers
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname, 'public')));

//setting view path
app.set('/views', path.join(__dirname, "views"));

//using api
app.use('/api/feeds', feedsApi);
app.use('/api/user', userApi);
app.use('/api/comment', commentApi);


// app.post('/api/feed/postFeedImage', feedController.postFeedImage);


app.get('/', function(req, res, next) {
  // res.render('index', { title: 'Express' });
  res.sendFile(path.join(__dirname, 'views/index.html'));
});


app.listen(3000, function(req, res){
	console.log('Listenting on localhost:3000');
});

