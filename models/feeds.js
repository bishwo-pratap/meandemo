var mongoose = require('mongoose');

var FeedsSchema = new mongoose.Schema({
	image: {
		type: String,
		required: false
	},
	feed: {
		type: String,
		required: true
	},
	created_at: {
		type: String,
		required: true
	},
	updated_at: {
		type: String,
		required: true
	},
	created_by: {
		type: String,
		required: true
	},
	created_by_name: {
		type: String,
		required: false
	}
});

FeedsSchema.set('collection','Feeds');

module.exports = mongoose.model('Feeds', FeedsSchema);