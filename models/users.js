var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: false
	}
});

UserSchema.set('collection','Users');
module.exports = mongoose.model('Users',UserSchema);