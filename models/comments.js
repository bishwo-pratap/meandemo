var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
	uname: {
		type: String,
		required: false
	},
	uid
	: {
		type: String,
		required: false
	},
	pid: {
		type: mongoose.Schema.ObjectId,
		required: true
	},
	comment: { 
		type: String,
		required: true
	},
	created_at: {
		type: String,
		required: true
	},
	updated_at: {
		type: String,
		required: false
	},
});

CommentSchema.set('collection','Comments');

module.exports = mongoose.model('Comments',CommentSchema);