var mongoose = require('mongoose');

/*Creating schema*/
var LikesSchema = new mongoose.Schema({
	uid: {
		type: String,
		required: false
	},
	pid: {
		type: mongoose.Schema.ObjectId,
		required: false
	},
	like: { 
		type: Boolean
	},
	time: {
		type: String
	}
});

LikesSchema.set('collection', 'Likes');

module.exports = mongoose.model('Likes',LikesSchema);